# Flags de compilación
CC = gcc
CFLAGS = -O3 -Werror -Wall -pedantic -Wextra -Wconversion -Wno-unused-parameter -std=gnu11 -D _GNU_SOURCE

# Binarios
USERS_BIN = users_service
FILES_BIN = files_service

# Directorios
SRC_DIR = src
BIN_DIR = bin
CFG_DIR = conf
LOG_DIR = /var/log/so2-tp3
SYS_DIR = /etc/systemd/system
DATA_DIR = /data
NGINX_DIR = /etc/nginx
INSTALL_DIR = /usr/bin/so2-tp3
SUDOERS_DIR = /etc/sudoers.d/

# Nginx
NGINX_FILE = nginx-conf.conf

# Daemons
DAEMONS = daemon-users.service daemon-files.service

default : all

all : tp3
	cppcheck $(SRC_DIR) --enable=warning,performance,portability,style --language=c
	@echo "Compilación exitosa | ${shell date --iso=seconds}"

install : ulfius_dependencies_install ulfius nginx awscli tp3 create_users
	mkdir -p $(INSTALL_DIR)
	mkdir -p --mode=u+rwx,g+rwxs,o-wx  $(LOG_DIR)
	mkdir -p $(DATA_DIR)
	cp $(BIN_DIR)/* $(INSTALL_DIR)
	chown tp3-users $(INSTALL_DIR)/$(USERS_BIN)
	chown tp3-files $(INSTALL_DIR)/$(FILES_BIN)
	chgrp tp3-users $(LOG_DIR)
	sudo setfacl -Rdm g:tp3-users:rwx $(LOG_DIR)
	cp $(CFG_DIR)/$(NGINX_FILE) $(NGINX_DIR)/sites-available
	ln -f -s $(NGINX_DIR)/sites-available/$(NGINX_FILE) $(NGINX_DIR)/sites-enabled/
	htpasswd -c -m $(CFG_DIR)/.htpasswd admin
	rm /etc/nginx/sites-enabled/default 
	cp $(CFG_DIR)/daemon* $(SYS_DIR)
	cp $(CFG_DIR)/.htpasswd $(NGINX_DIR)
	cp $(CFG_DIR)/sudoers $(SUDOERS_DIR)
	systemctl daemon-reload
	systemctl start $(DAEMONS)
	systemctl enable $(DAEMONS)
	service nginx reload

reinstall : tp3
	systemctl stop $(DAEMONS)
	cp $(BIN_DIR)/* $(INSTALL_DIR)
	chown tp3-users $(INSTALL_DIR)/$(USERS_BIN)
	chown tp3-files $(INSTALL_DIR)/$(FILES_BIN)
	cp $(CFG_DIR)/$(NGINX_FILE) $(NGINX_DIR)/sites-available
	ln -f -s $(NGINX_DIR)/sites-available/$(NGINX_FILE) $(NGINX_DIR)/sites-enabled/
	cp $(CFG_DIR)/daemon* $(SYS_DIR)
	cp $(CFG_DIR)/.htpasswd $(NGINX_DIR)
	cp $(CFG_DIR)/sudoers $(SUDOERS_DIR)
	systemctl daemon-reload
	systemctl start $(DAEMONS)
	systemctl enable $(DAEMONS)
	service nginx reload

tp3 : $(SRC_DIR)/users_service.c # $(SRC_DIR)/files_service.c
	mkdir -p $(BIN_DIR)
	$(CC) $(CFLAGS)  -o $(BIN_DIR)/$(USERS_BIN) $(SRC_DIR)/users_service.c -lulfius -ljansson -lcrypt
	$(CC) $(CFLAGS)  -o $(BIN_DIR)/$(FILES_BIN) $(SRC_DIR)/files_service.c -lulfius -ljansson

create_users:
	sudo groupadd tp3-users
	sudo useradd tp3-users -p 12345 -g tp3-users -s /bin/bash
	sudo useradd tp3-files -p 12345 -g tp3-users -s /bin/bash

ulfius_dependencies_install:
	./conf/ulfius_dependencies_install.sh

ulfius:
	./conf/ulfius_install.sh

nginx:
	./conf/nginx_install.sh

awscli:
	./conf/awscli_install.sh

clean :
	rm  -Rf $(BINARY_DIR)
