//
// Created by lucas on 22/5/21.
//

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <errno.h>

#include <ulfius.h>
#include <jansson.h>
#include <pwd.h>

#include <locale.h>
#include <time.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "../src/logger.c"

#define PORT                8082
#define BUFFER_SIZE         1024
#define FILENAME_SIZE       80
#define HEADER_SIZE         30
#define PRODUCT_SIZE        13
#define DATE_SIZE           11
#define BASE_LINK           "http://tp3-lmonsierra.com/data/"
#define DATA_DIR            "/data"
#define SERVICE_NAME        "Servicio de Archivos"
#define INPUT_DATE_FORMAT   "%Y%m%d%H"
#define DELIM               ":"
#define REQ_HEADER          "application/json"

DIR *d;
FILE *fd;
time_t t;

/* Declaración de funciones */
int32_t callbackGetFiles(const struct _u_request *request, struct _u_response *response, void *user_data);

void getFileLink(char *link, char *filename);

int getFilesize(char *size, char *filename);

void addFile2Json(json_t *data, int32_t fileId, char *fileLink, char *filesize);

int32_t callbackPostFiles(const struct _u_request *request, struct _u_response *response, void *user_data);

int checkRequestBody(json_t *body);

void generateLocation(json_t *body, char *location);

int32_t generateFilename(char *filename, char *location);

int32_t checkExistFile(json_t *response, char *filename);

int32_t checkDownloadingFile(char *filename);

int32_t downloadNewFile(char *location, char *filename);

int32_t getFilesNumber();

int32_t headersChecker(const struct _u_map *map);

/* Función principal */
int main(void) {
    struct _u_instance instance;

    // Initialize instance with the port number
    if (ulfius_init_instance(&instance, PORT, NULL, NULL) != U_OK) {
        fprintf(stderr, "Error ulfius_init_instance, abort\n");
        return (EXIT_FAILURE);
    }

    // Endpoint list declaration
    ulfius_add_endpoint_by_val(&instance, "GET", "api/servers/get_goes", NULL, 0, &callbackGetFiles, NULL);
    ulfius_add_endpoint_by_val(&instance, "POST", "api/servers/get_goes", NULL, 0, &callbackPostFiles, NULL);

    // Start the framework
    if (ulfius_start_framework(&instance) == U_OK) {
        printf("Start framework on port %d\n", instance.port);
        // Wait for the user to press <enter> on the console to quit the application
        pause();
    } else {
        fprintf(stderr, "Error starting framework\n");
    }

    printf("End framework\n");

    ulfius_stop_framework(&instance);
    ulfius_clean_instance(&instance);

    return EXIT_SUCCESS;
}

/**
 * Servicio que obtiene la lista de archivos descargados al momento en el sistema
 * @param request
 * @param response
 * @param user_data
 * @return 0 en caso de éxito, distinto de 0 en caso contrario
 */
int32_t callbackGetFiles(const struct _u_request *request, struct _u_response *response, void *user_data) {

    json_t *response_body = json_object();
    json_t *data = json_array();

    /* Verifico que los headers recibidos sean correctos */
    if (headersChecker(request->map_header)) {
        json_object_set(response_body, "description", json_string("Bad Request. The request has an invalid header"));
        ulfius_set_json_body_response(response, 400, response_body);
        return U_CALLBACK_CONTINUE;
    }


    /* Abro el directorio donde se almacenan los archivos descargados */
    if ((d = opendir(DATA_DIR)) == NULL) {
        fprintf(stderr, "Error al abrir el directorio de usuarios. %s\n", strerror(errno));
        return U_CALLBACK_ERROR;
    }

    /* Leo el listado de archivos descargados */
    struct dirent *dir;
    char *size = calloc(FILENAME_SIZE, sizeof(char));
    char *link = calloc(BUFFER_SIZE, sizeof(char));
    while ((dir = readdir(d)) != NULL) {
        /* Verifico que se trate de un archivo regular para no listar directorios */
        if ((dir->d_type == DT_REG) && (getFilesize(size, dir->d_name) == 0)) {
            getFileLink(link, dir->d_name);
            addFile2Json(data, (int32_t) dir->d_ino, link, size);
        }
    }

    free(size);
    closedir(d);

    /* Seteo el listado de usuarios en formato JSON como body */
    json_object_set(response_body, "data", data);
    ulfius_set_json_body_response(response, 200, response_body);

    /* Logueo el resultado */
    sprintf(link, "Archivos en el server: %i", (int16_t) json_array_size(data));
    logger(link, SERVICE_NAME);

    free(link);
    return U_CALLBACK_CONTINUE;
}

/**
 * Almacena en un buffer el link de descarga del archivo indicado por parámetro
 * @param link      buffer donde se almacena el link del archivo
 * @param filename  nombre del archivo a setear el link
 */
void getFileLink(char *link, char *filename) {
    sprintf(link, "%s%s", BASE_LINK, filename);
}

/**
 * Almacena en un buffer el tamaño del archivo indicado por parámetro expresado en bytes
 * @param size      buffer donde se almacena el tamaño del archivo formateado
 * @param filename  nombre del archivo a calcular el tamaño
 * @return 0 en caso de éxito, el valor del errno en caso de error
 */
int getFilesize(char *size, char *filename) {
    struct stat statbuf;
    char *buffer = calloc(BUFFER_SIZE, sizeof(char));
    sprintf(buffer, "%s/%s", DATA_DIR, filename);
    if (stat(buffer, &statbuf) == 0) {
        sprintf(size, "%i bytes", (int) statbuf.st_size);
    } else {
        fprintf(stderr, "Se produjo un error al obtener el tamaño del archivo [%s][%s]", filename, strerror(errno));
    }
    free(buffer);
    return errno;
}

/**
 * Agrega un archivo a la estructura data en formato JSON
 * @param data      estructura que almacena el listado de archivos
 * @param fileId    id del archivo a insertar
 * @param fileLink  link de descarga del archivo a insertar
 * @param filesize  tamaño en bytes del archivo a insertar
 */
void addFile2Json(json_t *data, int32_t fileId, char *fileLink, char *filesize) {
    json_t *file = json_object();
    json_object_set(file, "file_id", json_integer(fileId));
    json_object_set(file, "link", json_string(fileLink));
    json_object_set(file, "filesize", json_string(filesize));
    json_array_append(data, file);
}

/**
 * Servicio que obtiene el link de descarga del archivo especificado por request o lo descarga en caso
 * de que no exista en el servidor
 * @param request
 * @param response
 * @param user_data
 * @return 0 en caso de éxito, distinto de 0 en caso contrario
 */
int32_t callbackPostFiles(const struct _u_request *request, struct _u_response *response, void *user_data) {

    json_t *request_body = ulfius_get_json_body_request(request, NULL);
    json_t *response_body = json_object();
    int32_t newFile = 0;

    /* Verifico que los headers recibidos sean correctos */
    if (headersChecker(request->map_header)) {
        json_object_set(response_body, "description", json_string("Bad Request. The request has an invalid header"));
        ulfius_set_json_body_response(response, 400, response_body);
        return U_CALLBACK_CONTINUE;
    }

    /* Verifico validez del body. Retorno Bad request [400] en caso de fallo */
    if (checkRequestBody(request_body) != 0) {
        json_object_set(response_body, "description", json_string("Bad Request"));
        ulfius_set_json_body_response(response, 400, response_body);
        return U_CALLBACK_CONTINUE;
    }

    char *filename = calloc(FILENAME_SIZE, sizeof(char));
    char *location = calloc(FILENAME_SIZE, sizeof(char));
    generateLocation(request_body, location);

    /* Verifico que el archivo solicitado exista en el bucket S3 y obtengo su nombre y tamaño */
    if (generateFilename(filename, location) != 0) {
        json_object_set(response_body, "description", json_string("File not found in S3"));
        ulfius_set_json_body_response(response, 404, response_body);
        return U_CALLBACK_CONTINUE;
    }

    /* Verifico si el archivo solicitado ya fue descargado anteriormente */
    if (checkExistFile(response_body, filename) != 0) {
        /* Verifico si el archivo se está descargando actualmente, sino lo descargo*/
        if (checkDownloadingFile(filename) == 0) {
            json_object_set(response_body, "description",
                            json_string("File still downloading. Please wait some minutes..."));
            ulfius_set_json_body_response(response, 200, response_body);
        } else {
            json_object_set(response_body, "description", json_string("Downloading requested file..."));
            ulfius_set_json_body_response(response, 200, response_body);
            if (downloadNewFile(location, filename) != 0) {
                json_object_set(response_body, "description",
                                json_string("An error occurred while download file. Try again in few seconds..."));
                ulfius_set_json_body_response(response, 500, response_body);
            } else {
                newFile++;
            }
        }
    } else {
        /* Envío la respuesta en caso que se haya encontrado el archivo */
        ulfius_set_json_body_response(response, 200, response_body);
    }

    free(location);
    free(filename);

    /* Logueo los archivos descargados nuevos y preexistentes */
    char *buffer = calloc(BUFFER_SIZE, sizeof(char));
    sprintf(buffer, "Archivos descargados nuevos: %i", newFile);
    logger(buffer, SERVICE_NAME);
    sprintf(buffer, "Archivos descargados preexistentes: %i", getFilesNumber());
    logger(buffer, SERVICE_NAME);
    free(buffer);
    return U_CALLBACK_CONTINUE;
}

/**
 * Verifica que el body recibido contenga la información necesaria ('product' y 'datetime')
 * @param body      body recibido en la solicitud del cliente
 * @return 0 si el body es correcto, distinto de 0 en caso contrario
 */
int checkRequestBody(json_t *body) {
    /* Verifico que los datos 'product' y 'datetime' del body sean string */
    if (json_is_string(json_object_get(body, "product")) && json_is_string(json_object_get(body, "datetime"))) {
        const char *input_product = json_string_value(json_object_get(body, "product"));
        const char *input_datetime = json_string_value(json_object_get(body, "datetime"));

        /* Verifico que los datos no sean mayor al límite y sólo contenga caracteres válidos */
        char valid_product[PRODUCT_SIZE];
        char valid_datetime[DATE_SIZE];
        sscanf(input_product, "%13[a-zA-Z0-9%-_]", valid_product);
        sscanf(input_datetime, "%10[0-9]", valid_datetime);
        return (strcmp(input_product, valid_product) != 0) || (strcmp(input_datetime, valid_datetime) != 0);
    }
    return -1;
}

/**
 * Obtiene la localización del archivo solicitado en la request en el bucket S3
 * @param body      body recibido en la solicitud del cliente
 * @param location  buffer que almacena la localización del archivo solicitado en el bucket S3
 */
void generateLocation(json_t *body, char *location) {

    const char *product = json_string_value(json_object_get(body, "product"));
    const char *datetime = json_string_value(json_object_get(body, "datetime"));

    struct tm datetime_format;
    char *year = calloc(DATE_SIZE, sizeof(char));
    char *dayOfYear = calloc(DATE_SIZE, sizeof(char));
    char *hour = calloc(DATE_SIZE, sizeof(char));

    strptime(datetime, INPUT_DATE_FORMAT, &datetime_format);
    strftime(year, DATE_SIZE, "%Y", &datetime_format);
    strftime(dayOfYear, DATE_SIZE, "%j", &datetime_format);
    strftime(hour, DATE_SIZE, "%H", &datetime_format);

    sprintf(location, "%s/%s/%s/%s", product, year, dayOfYear, hour);

    free(year);
    free(dayOfYear);
    free(hour);
}

/**
 * Obtiene el nombre del archivo solicitado para buscarlo en el servidor o descargarlo
 * @param filename  buffer que almacena el nombre del archivo solicitado
 * @param location  buffer que almacena la localización del archivo solicitado en el bucket S3
 * @return 0 en caso de éxito, -1 en aso contrario
 */
int32_t generateFilename(char *filename, char *location) {

    /* Genero el comando a ejecutar */
    char *buffer = calloc(BUFFER_SIZE, sizeof(char));
    sprintf(buffer, "aws s3 ls noaa-goes16/%s/ --no-sign-request | sort | head -n 1 | awk '{print $4}'",
            location);

    /* Ejecuto el comando generado */
    if ((fd = popen(buffer, "r")) == NULL) {
        /* Logueo el error correspondiente */
        sprintf(buffer, "Error al intentar consultar la información del archivo solicitado [%s][%s]", location,
                strerror(errno));
        fprintf(stderr, "%s\n", buffer);
        logger(buffer, SERVICE_NAME);
        free(buffer);
        return -1;
    }

    if (fgets(buffer, BUFFER_SIZE, fd) == NULL) {
        /* Logueo el error correspondiente */
        sprintf(buffer, "No se pudo obtener la información archivo solicitado [%s]", location);
        fprintf(stderr, "%s\n", buffer);
        logger(buffer, SERVICE_NAME);
        free(buffer);
        return -1;
    } else {
        sprintf(filename, "%s", strtok(buffer, "\n"));
        free(buffer);
        pclose(fd);
        return 0;
    }
}

/**
 * Verifica si el archivo solicitado ya fue descargado en el servidor
 * @param response  estrucura que almacena los datos actuales del archivo solicitado en caso que exista
 * @param filename  nombre del archivo solicitado
 * @return 0 en caso de éxito, distinto de 0 en aso contrario
 */
int32_t checkExistFile(json_t *response, char *filename) {
    /* Abro el directorio donde se almacenan los archivos descargados */
    if ((d = opendir(DATA_DIR)) == NULL) {
        fprintf(stderr, "Error al abrir el directorio de archivos. %s\n", strerror(errno));
        return U_CALLBACK_ERROR;
    }

    /* Leo el listado de archivos descargados */
    struct dirent *dir;
    char *size = calloc(FILENAME_SIZE, sizeof(char));
    char *link = calloc(BUFFER_SIZE, sizeof(char));
    while ((dir = readdir(d)) != NULL) {
        /* Verifico que se trate de un archivo regular y del sombre solicitado */
        if ((dir->d_type == DT_REG) && (strcmp(filename, dir->d_name) == 0)) {
            getFileLink(link, dir->d_name);
            getFilesize(size, dir->d_name);
            json_object_set(response, "file_id", json_integer((int) dir->d_ino));
            json_object_set(response, "link", json_string(link));
            json_object_set(response, "filesize", json_string(size));
            closedir(d);
            return 0;
        }
    }
    closedir(d);
    return -1;
}

/**
 * Verifica si el archivo solicitado está siendo descargado en background
 * @param filename  nombre del archivo solicitado
 * @return 0 en caso de éxito (el archivo se está descargando), distinto de 0 en aso contrario
 */
int32_t checkDownloadingFile(char *filename) {
    /* Abro el directorio donde se almacenan los archivos descargados */
    if ((d = opendir(DATA_DIR)) == NULL) {
        fprintf(stderr, "Error al abrir el directorio de archivos. %s\n", strerror(errno));
        return U_CALLBACK_ERROR;
    }

    char *buffer = calloc(FILENAME_SIZE, sizeof(char));
    strcpy(buffer, filename);
    strtok(buffer, ".");

    /* Leo el listado de archivos descargados */
    struct dirent *dir;
    while ((dir = readdir(d)) != NULL) {
        /* Verifico si existe el archivo temporal solicitado */
        if ((dir->d_type == DT_REG) && (strcmp(buffer, strtok(dir->d_name, ".")) == 0)) {
            free(buffer);
            closedir(d);
            return 0;
        }
    }

    free(buffer);
    closedir(d);
    return -1;
}

/**
 * Inicia la descarga del archivo solicitado en background
 * @param location  buffer que almacena la localización del archivo solicitado en el bucket S3
 * @param filename  nombre del archivo solicitado
 * @return 0 en caso de éxito, -1 en aso contrario
 */
int32_t downloadNewFile(char *location, char *filename) {

    /* Genero el comando a ejecutar */
    char *buffer = calloc(BUFFER_SIZE, sizeof(char));
    sprintf(buffer, "sudo aws s3 cp s3://noaa-goes16/%s/%s %s --no-sign-request&", location, filename, DATA_DIR);

    /* Ejecuto el comando generado */
    if ((fd = popen(buffer, "r")) == NULL) {
        /* Logueo el error correspondiente */
        sprintf(buffer, "Error al intentar consultar la información del archivo solicitado [%s][%s]", location,
                strerror(errno));
        fprintf(stderr, "%s\n", buffer);
        logger(buffer, SERVICE_NAME);
        free(buffer);
        pclose(fd);
        return -1;
    }

    free(buffer);
    pclose(fd);
    return 0;
}

/**
 * Obtiene la cantidad de archivos descargados preexistentes
 * @return número de archivos descargados preexistentes
 */
int32_t getFilesNumber() {
    /* Abro el directorio donde se almacenan los archivos descargados */
    if ((d = opendir(DATA_DIR)) == NULL) {
        fprintf(stderr, "Error al abrir el directorio de archivos. %s\n", strerror(errno));
        return U_CALLBACK_ERROR;
    }

    /* Leo el listado de archivos descargados */
    int32_t files = 0;
    struct dirent *dir;
    while ((dir = readdir(d)) != NULL) {
        /* Verifico que se trate de un archivo regular y cuento */
        if (dir->d_type == DT_REG) {
            files++;
        }
    }
    closedir(d);
    return files;
}

/**
 * Verifica que el request recibido contenga los headers necesarios
 * @param request   request recibido en la solicitud del cliente
 * @return 0 si el request es correcto, 1 en caso contrario
 */
int32_t headersChecker(const struct _u_map *map) {
    /* Verifico  longitud de los headers */
    if ((u_map_get_case_length(map, "accept") > HEADER_SIZE) ||
        (u_map_get_case_length(map, "content-type") > HEADER_SIZE)) {
        return 1;
    }

    const char *acceptHeader = u_map_get_case(map, "accept");
    const char *contentTypeHeader = u_map_get_case(map, "content-type");

    if ((acceptHeader != NULL) && (contentTypeHeader != NULL)) {
        return ((strcmp(acceptHeader, REQ_HEADER) != 0) || (strcmp(contentTypeHeader, REQ_HEADER) != 0));
    }

    return 1;
}

