//
// Created by lucas on 21/5/21.
//

#include <time.h>
#include <errno.h>
#include "../lib/logger.h"

#define LOG_FILE            "/var/log/so2-tp3/services.log"

FILE *fd;
time_t t;

/**
 * Loguea mensajes en el archivo user.log ubicado en la carpeta "log"
 * El formato generado es [timestamp] | [nombre del servicio] | [mensaje]
 * @param msg       mensaje a loguear
 * @param servicio  servicio que genera el log
 */
void logger(char *msg, char *servicio) {
    if ((fd = fopen(LOG_FILE, "a")) == 0) {
        fprintf(stderr, "Error al abrir el archivo de log. Servicio: [%s][%s]\n", servicio, strerror(errno));
    } else {
        time(&t);
        fprintf(fd, "[%s] | [%s] | [%s]\n", strtok(ctime(&t), "\n"), servicio, msg);
        fclose(fd);
    }
}