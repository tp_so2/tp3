//
// Created by lucas on 21/5/21.
//

#include <stdio.h>
#include <ulfius.h>
#include <jansson.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>

#include <crypt.h>
#include <pwd.h>
#include <time.h>
#include "../src/logger.c"

#define PORT                8081
#define BUFFER_SIZE         1024
#define USERNAME_SIZE       33
#define HEADER_SIZE         30
#define SERVICE_NAME        "Servicio de Usuarios"
#define DELIM               ":"
#define REQ_HEADER          "application/json"

FILE *fd;
time_t t;

/* Declaración de funciones */
int32_t callbackGetUsers(const struct _u_request *request, struct _u_response *response, void *user_data);

void addUser2Json(json_t *data, int16_t userId, char *username);

int32_t callbackPostUsers(const struct _u_request *request, struct _u_response *response, void *user_data);

int32_t checkRequestBody(json_t *body);

int32_t createNewUser(json_t *json);

char *decodeExitCode(int32_t exitCode);

int32_t getUidByUsername(const char *username);

int32_t headersChecker(const struct _u_map *map);

/* Función principal */
int main(void) {
    struct _u_instance instance;

    // Initialize instance with the port number
    if (ulfius_init_instance(&instance, PORT, NULL, NULL) != U_OK) {
        fprintf(stderr, "Error ulfius_init_instance, abort\n");
        return (EXIT_FAILURE);
    }

    // Endpoint list declaration
    ulfius_add_endpoint_by_val(&instance, "GET", "api/users", NULL, 0, &callbackGetUsers, NULL);
    ulfius_add_endpoint_by_val(&instance, "POST", "api/users", NULL, 0, &callbackPostUsers, NULL);

    // Start the framework
    if (ulfius_start_framework(&instance) == U_OK) {
        printf("Start framework on port %d\n", instance.port);
        // Wait for the user to press <enter> on the console to quit the application
        pause();
    } else {
        fprintf(stderr, "Error starting framework\n");
    }

    printf("End framework\n");

    ulfius_stop_framework(&instance);
    ulfius_clean_instance(&instance);

    return EXIT_SUCCESS;
}

/**
 * Servicio que obtiene la lista de usuarios del sistema
 * @param request
 * @param response
 * @param user_data
 * @return 0 en caso de éxito, distinto de 0 en caso contrario
 */
int32_t callbackGetUsers(const struct _u_request *request, struct _u_response *response, void *user_data) {

    json_t *response_body = json_object();
    json_t *data = json_array();

    /* Verifico que los headers recibidos sean correctos */
    if (headersChecker(request->map_header)) {
        json_object_set(response_body, "description", json_string("Bad Request. The request has an invalid header"));
        ulfius_set_json_body_response(response, 400, response_body);
        return U_CALLBACK_CONTINUE;
    }

    /* Abro el archivo de usuarios en modo lectura */
    if ((fd = fopen("/etc/passwd", "r")) == NULL) {
        fprintf(stderr, "Error al abrir el archivo de usuarios\n");
        return U_CALLBACK_ERROR;
    }

    /* Leo el listado de usuarios */
    char *buffer = calloc(BUFFER_SIZE, sizeof(char));
    while (fgets(buffer, BUFFER_SIZE, fd) != NULL) {
        char *user = strtok(buffer, DELIM);
        strtok(NULL, DELIM);
        int16_t id = (int16_t) strtol(strtok(NULL, DELIM), NULL, 10);
        addUser2Json(data, id, user);
    }

    fclose(fd);

    /* Seteo el listado de usuarios en formato JSON como body */
    json_object_set(response_body, "data", data);
    ulfius_set_json_body_response(response, 200, response_body);

    /* Logueo el resultado */
    sprintf(buffer, "Usuarios listados: %i", (int16_t) json_array_size(data));
    logger(buffer, SERVICE_NAME);

    free(buffer);
    return U_CALLBACK_CONTINUE;
}

/**
 * Agrega un usuario a la estructura data en formato JSON
 * @param data      estructura que almacena los usuarios
 * @param userId    id del usuario a insertar
 * @param username  nombre de usuario a insertar
 */
void addUser2Json(json_t *data, int16_t userId, char *username) {
    json_t *user = json_object();
    json_object_set(user, "user_id", json_integer(userId));
    json_object_set(user, "username", json_string(username));
    json_array_append(data, user);
}

/**
 * Servicio que crea un nuevo usuario en el SO del servidor
 * @param request
 * @param response
 * @param user_data
 * @return 0 en caso de éxito, distinto de 0 en caso contrario
 */
int32_t callbackPostUsers(const struct _u_request *request, struct _u_response *response, void *user_data) {

    json_t *request_body = ulfius_get_json_body_request(request, NULL);
    json_t *response_body = json_object();

    /* Verifico que los headers recibidos sean correctos */
    if (headersChecker(request->map_header)) {
        json_object_set(response_body, "description", json_string("Bad Request. The request has an invalid header"));
        ulfius_set_json_body_response(response, 400, response_body);
        return U_CALLBACK_CONTINUE;
    }

    /* Verifico validez del body. Retorno Bad request [400] en caso de fallo */
    if (checkRequestBody(request_body) != 0) {
        json_object_set(response_body, "description", json_string("Bad Request"));
        ulfius_set_json_body_response(response, 400, response_body);
        return U_CALLBACK_CONTINUE;
    }

    int32_t userId;
    /* Creo y verifico que el usuario se haya creado correctamente */
    if ((userId = createNewUser(request_body)) < 0) {
        json_object_set(response_body, "description", json_string("Ocurrió un error al crear el usuario"));
        ulfius_set_json_body_response(response, 409, response_body);
        return U_CALLBACK_CONTINUE;
    }

    /* Armo el response */
    time(&t);
    json_object_set(response_body, "id", json_integer(userId));
    json_object_set(response_body, "username", json_object_get(request_body, "username"));
    json_object_set(response_body, "created_at", json_string(strtok(ctime(&t), "\n")));

    ulfius_set_json_body_response(response, 200, response_body);

    char *buffer = calloc(BUFFER_SIZE, sizeof(char));
    sprintf(buffer, "Usuario %i creado", userId);
    logger(buffer, SERVICE_NAME);
    free(buffer);
    return U_CALLBACK_CONTINUE;
}

/**
 * Verifica que el body recibido contenga la información necesaria ('username' y 'password')
 * @param body      body recibido en la solicitud del cliente
 * @return 0 si el body es correcto, distinto de 0 en caso contrario
 */
int checkRequestBody(json_t *body) {
    /* Verifico que los datos 'username' y 'password' del body sean string */
    if (json_is_string(json_object_get(body, "username")) && json_is_string(json_object_get(body, "password"))) {
        const char *input_username = json_string_value(json_object_get(body, "username"));
        char valid_username[USERNAME_SIZE];

        /* Verifico que el username no sea mayor al límite y sólo contenga caracteres válidos */
        sscanf(input_username, "%32[a-zA-Z0-9]", valid_username);
        return strcmp(input_username, valid_username);
    }
    return -1;
}

/**
 * Crea un nuevo usuario de SO
 * @param json      Contiene el usuario y la contraseña del usuario que se va a crear
 * @return ID del nuevo usuario, -1 en caso que no se haya podido crear el usuario
 */
int32_t createNewUser(json_t *json) {

    const char *username = json_string_value(json_object_get(json, "username"));
    const char *password = crypt(json_string_value(json_object_get(json, "password")), "k8");

    char *buffer = calloc(BUFFER_SIZE, sizeof(char));
    sprintf(buffer, "sudo useradd %s -p %s -s /bin/rbash -m", username, password);

    /* Hago la llamada a sistema para crear el nuevo usuario */
    int32_t exitCode = system(buffer);
    if (exitCode != 0) {
        /* Logueo el error correspondiente */
        sprintf(buffer, "Error al intentar crear el usuario [%s][%s]", username, decodeExitCode(exitCode));
        fprintf(stderr, "%s\n", buffer);
        logger(buffer, SERVICE_NAME);
        free(buffer);
        return -1;
    } else {
        free(buffer);
        return getUidByUsername(username);
    }
}

/**
 * Decodifica el código retornado por la llamada a sistema para la creación de usuarios
 * @param exitCode      código retornado por la llamada a sistemna 'useradd'
 * @return string con el mensaje correspondiente
 */
char *decodeExitCode(int32_t exitCode) {
    switch (exitCode) {
        case 1:
            return "Can't update password file";
        case 2:
            return "Invalid command syntax";
        case 3:
            return "Invalid argument to option";
        case 4:
            return "UID already in use";
        case 6:
            return "Specified group doesn't exist";
        case 9:
            return "Username already in use";
        case 10:
            return "Can't update group file";
        case 12:
            return "Can't create home directory";
        case 13:
            return "Can't create mail spool";
        case 14:
            return "Can't update SELinux user mapping";
        default:
            return "Failure";
    }
}

/**
 * Retorna el ID de usuario pasado por parámetro
 * @param username      nombre del usuario creado
 * @return ID del usuario
 */
int32_t getUidByUsername(const char *username) {
    struct passwd *pwd = calloc(1, sizeof(struct passwd));
    size_t buffer_len = (size_t) sysconf(_SC_GETPW_R_SIZE_MAX) * sizeof(char);
    char *buffer = calloc(1, buffer_len);

    /* Obtengo el ID del usuario a partir del username */
    getpwnam_r(username, pwd, buffer, buffer_len, &pwd);
    free(pwd);
    free(buffer);
    return (int32_t) pwd->pw_uid;
}

/**
 * Verifica que el request recibido contenga los headers necesarios
 * @param request   request recibido en la solicitud del cliente
 * @return 0 si el request es correcto, 1 en caso contrario
 */
int32_t headersChecker(const struct _u_map *map) {
    /* Verifico  longitud de los headers */
    if ((u_map_get_case_length(map, "accept") > HEADER_SIZE) ||
        (u_map_get_case_length(map, "content-type") > HEADER_SIZE)) {
        return 1;
    }

    const char *acceptHeader = u_map_get_case(map, "accept");
    const char *contentTypeHeader = u_map_get_case(map, "content-type");

    if ((acceptHeader != NULL) && (contentTypeHeader != NULL)) {
        return ((strcmp(acceptHeader, REQ_HEADER) != 0) || (strcmp(contentTypeHeader, REQ_HEADER) != 0));
    }

    return 1;
}
